package com.sgo.populaterealmapp.BBS;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.sgo.populaterealmapp.MyApiClient;
import com.sgo.populaterealmapp.BuildConfig;
import com.sgo.populaterealmapp.NoHPFormat;
import com.sgo.populaterealmapp.R;

public class MainActivity extends AppCompatActivity {

    AlertDialog d;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bbs);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        if (toolbar != null) {
            TextView tv_typeApp = (TextView) toolbar.findViewById(R.id.text_typeApp);
            tv_typeApp.setText(BuildConfig.FLAVOR);
        }

        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        if (fab != null) {
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            });
        }

        InitializeAlertDialog();
        if(d != null)
            d.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            if(d!=null)
                d.show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void InitializeAlertDialog() {
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.layout_dialog_userid, null);
        d = new AlertDialog.Builder(this)
                .setView(dialogView)
                .setCancelable(false)
                .setTitle("User ID")
                .setPositiveButton(android.R.string.ok, null) //Set to null. We override the onclick
                .setNegativeButton(android.R.string.cancel, null)
                .create();

        final EditText dBt = (EditText)dialogView.findViewById(R.id.edit1);

        d.setOnShowListener(new DialogInterface.OnShowListener() {

            @Override
            public void onShow(DialogInterface dialog) {

                Button b = d.getButton(AlertDialog.BUTTON_POSITIVE);
                b.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        // TODO Do something

                        MyApiClient.USER_ID = NoHPFormat.editNoHP(dBt.getText().toString());
                        if(!MyApiClient.USER_ID.isEmpty())
                            d.dismiss();
                    }
                });
            }
        });
    }
}
