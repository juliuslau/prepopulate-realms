package com.sgo.populaterealmapp.BBSMember;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.ResultReceiver;
import android.util.Log;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.sgo.populaterealmapp.BBS.Beans.BBSBankModel;
import com.sgo.populaterealmapp.Biller.DateTimeFormat;
import com.sgo.populaterealmapp.MyApiClient;
import com.sgo.populaterealmapp.WebParams;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.Iterator;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.annotations.RealmModule;

/**
 * Created by yuddistirakiki on 5/10/16.
 * Intent Service yang membuat db realm.
 * semua prosesnya synchronous
 *
 */
public class BBSMemberPopulateService extends IntentService {

    public static final int DOWNLOAD_ERROR = 10;
    public static final int DOWNLOAD_SUCCESS = 11;
    public static final int DOWNLOAD_PROGRESS = 12;
    private final int SCHEME_CTA = 0;
    private final int SCHEME_ATC = 1;

    JSONArray cta;
    JSONArray atc;
    JSONObject bankCTA;
    JSONObject bankATC;

    ResultReceiver receiver;
    Realm realm;
    int appType;
    Bundle oi;
    File folder;
    File filePath;
    String app_id;

    @RealmModule(classes = { BBSBankModel.class})
    private static class BBSMemberModule {
    }

    public BBSMemberPopulateService() {
        super("BBSMemberPopulateService");
    }

    public BBSMemberPopulateService(String name) {
        super(name);
    }

    /**
     * Path dimana file realm nanti disimpan
     */
    private void createFolder(){
        folder = new File(Environment.getExternalStorageDirectory() +
                File.separator + "PopulateRealmApp");
        if (!folder.exists()) {
            folder.mkdir();
        }

    }

    @Override
    protected void onHandleIntent(Intent intent) {
        createFolder();

        appType = intent.getIntExtra("appType",0);
        oi = new Bundle();

        app_id = intent.getStringExtra("app_id");
        filePath = new File(folder.getPath(),intent.getStringExtra("realmName"));

        RealmConfiguration config = new RealmConfiguration.Builder()
                .directory(folder)
                .name(intent.getStringExtra("realmName"))
                .schemaVersion(intent.getIntExtra("realmVer",0))
                .modules(new BBSMemberModule())
                .migration(new BBSMemberRealMigration())
                .build();

        realm = Realm.getInstance(config);

        receiver = intent.getParcelableExtra("receiver");

        getBankProductBBS();
    }

    public void getBankProductBBS(){
        try{
            receiver.send(DOWNLOAD_PROGRESS,null);
            RequestParams params = new RequestParams();
            params.put(WebParams.RC_UUID, "123456");
            params.put(WebParams.RC_DTIME, "123456");
            params.put(WebParams.SIGNATURE, "123456");

            params.put(WebParams.APP_ID, app_id);
            Log.d("params list bank product bbs",params.toString());

            MyApiClient.getListBankProductList(this, true,params,new JsonHttpResponseHandler() {

                @Override
                public void onProgress(long bytesWritten, long totalSize) {
                    super.onProgress(bytesWritten, totalSize);
                    receiver.send(DOWNLOAD_PROGRESS,null);
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    receiver.send(DOWNLOAD_PROGRESS,null);
                    try {
                        String code = response.getString(WebParams.ERROR_CODE);
                        Log.d("Isi response list bank product bbs:" ,response.toString());
                        if (code.equals(WebParams.SUCCESS_CODE)) {
                            receiver.send(DOWNLOAD_PROGRESS,null);
                            InsertData(response);

                        } else {
                            code = response.getString(WebParams.ERROR_MESSAGE);
                            oi.putString("msg",code);
                            EndError(DOWNLOAD_ERROR,oi);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        oi.putString("msg","Exception Json");
                        EndError(DOWNLOAD_ERROR,oi);
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                private void failure(Throwable throwable) {
                    Log.w("Error Koneksi biller data list buy:" , throwable.toString());
                    oi.putString("msg",throwable.toString());
                    EndError(DOWNLOAD_ERROR,oi);
                }
            });
        }catch (Exception e){
            Log.d("httpclient:",e.getMessage());
            oi.putString("msg","Exception Json");
            EndError(DOWNLOAD_ERROR,oi);
        }
    }

    private void InsertData(JSONObject response){
        receiver.send(DOWNLOAD_PROGRESS,null);
        final String curr_date = DateTimeFormat.getCurrentDate();

        realm.beginTransaction();

        try {
            receiver.send(DOWNLOAD_PROGRESS,null);
            int i;
            int j;
            //clear data table BBSCommModel
            realm.delete(BBSBankModel.class);

            //prepare temp variable to insert to BBSBankModel
            BBSBankModel tempBBSbank;


            JSONObject community = response.getJSONObject("community");

            Iterator<String> iter = community.keys();
            String keys;
            String comm_type;
            String comm_scheme_code;
            JSONObject comm_object;
            JSONArray account_array;

            while (iter.hasNext()){
                keys = iter.next();
                comm_object = community.getJSONObject(keys);
                comm_scheme_code = comm_object.getString("scheme_code");
                if(comm_scheme_code.equalsIgnoreCase("ATC"))
                    comm_type = "SOURCE";
                else
                    comm_type = "BENEF";

                account_array = comm_object.getJSONArray("account");

                for(j = 0 ; j < account_array.length() ; j++){
                    tempBBSbank = realm.createObjectFromJson(BBSBankModel.class, account_array.getJSONObject(j));
                    tempBBSbank.setComm_type(comm_type);
                    tempBBSbank.setComm_id(comm_object.getString("comm_id"));
                    tempBBSbank.setScheme_code(comm_scheme_code);
                    tempBBSbank.setLast_update(curr_date);
                }

            }

            realm.commitTransaction();

            oi.putInt("appType",appType);
            EndError(DOWNLOAD_SUCCESS,oi);
        }
        catch (JSONException e){
            Log.e("error",e.getMessage());
            realm.cancelTransaction();
            oi.putString("msg",e.getMessage());
            EndError(DOWNLOAD_ERROR,oi);
        }
    }


//    /** Create a new file and save it to Drive. */
//    private void saveFileToDrive() {
//        // Start by creating a new contents, and setting a callback.
//        Log.i("BBSMemberPopulateService", "Creating new contents.");
//        DriveResourceClient driveResourceClient = Drive.getDriveResourceClient(this, GoogleSignIn.getLastSignedInAccount(this));
//        driveResourceClient
//                .createContents()
//                .continueWithTask(
//                        new Continuation<DriveContents, Task<Void>>() {
//                            @Override
//                            public Task<Void> then(@NonNull Task<DriveContents> task) throws Exception {
//                                return createFileIntentSender(task.getResult(), image);
//                            }
//                        })
//                .addOnFailureListener(
//                        new OnFailureListener() {
//                            @Override
//                            public void onFailure(@NonNull Exception e) {
//                                Log.w(TAG, "Failed to create new contents.", e);
//                            }
//                        });
//
//    }
//
//
//    private Task<Void> createFileIntentSender(DriveContents driveContents) {
//        DriveClient driveClient = Drive.getDriveClient(this, GoogleSignIn.getLastSignedInAccount(this));
//
//        // Get an output stream for the contents.
//        OutputStream outputStream = driveContents.getOutputStream();
//        // Write the bitmap data from it.
//
//
//        // Create the initial metadata - MIME type and title.
//        // Note that the user will be able to change the title later.
//        MetadataChangeSet metadataChangeSet =
//                new MetadataChangeSet.Builder()
//                        .setMimeType("image/jpeg")
//                        .setTitle("Android Photo.png")
//                        .build();
//        // Set up options to configure and display the create file activity.
//        CreateFileActivityOptions createFileActivityOptions =
//                new CreateFileActivityOptions.Builder()
//                        .setInitialMetadata(metadataChangeSet)
//                        .setInitialDriveContents(driveContents)
//                        .build();
//
//        return driveClient
//                .newCreateFileActivityIntentSender(createFileActivityOptions)
//                .continueWith(
//                        new Continuation<IntentSender, Void>() {
//                            @Override
//                            public Void then(@NonNull Task<IntentSender> task) throws Exception {
//                                startIntentSenderForResult(task.getResult(), REQUEST_CODE_CREATOR, null, 0, 0, 0);
//                                return null;
//                            }
//                        });
//    }


    private void EndRealm(){
        if(realm.isInTransaction())
            realm.cancelTransaction();

        if(realm != null && !realm.isClosed())
            realm.close();
    }

    private void EndError(int ReceiverTag, Bundle io){
        Bundle buk = (Bundle)io.clone();
        io.clear();
        EndRealm();
        receiver.send(ReceiverTag,buk);

    }
}
