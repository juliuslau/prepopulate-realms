package com.sgo.populaterealmapp.BBSMember;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.sgo.populaterealmapp.MyApiClient;
import com.sgo.populaterealmapp.Biller.PopulateService;
import com.sgo.populaterealmapp.R;
import com.sgo.populaterealmapp.ServiceReceiver;

/**
 * Fragment yg diimplemen di MainActivity
 * berisi layout/UI untuk membuat realm
 */
public class MainActivityFragment extends Fragment implements View.OnClickListener, ServiceReceiver.Receiver {

    private static final int PERMISSIONS_REQ_WRITEEXTERNALSTORAGE = 131;

    /**
     * Custom AppType biar nanti di service gak salah nentuin app mana yang sedang diproses
     */
    public static final int SALDOMU = 0x11;

    /**
     * version realm production
     * pastikan sama dengan di project
     */
    public static final int SALDOMU_VER_PROD = 0;

    /**
     * version realm development
     * pastikan sama dengan di project
     */
    public static final int SALDOMU_VER_DEV = 1;


    public static final String SALDOMU_APP_ID = "SDOM";

    private final String suffixDevName = "bbsmemberdev.realm";
    private final String suffixProdName = "bbsmember.realm";


    AlertDialog.Builder alertbox;
    ServiceReceiver resultReceiever;
    int button_id;
    ProgressBar mProgBar;
    TextView txt_saldomu;
    Switch sw_prod;
    int mProgress;


    public MainActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View v = inflater.inflate(R.layout.fragment_bbs_member, container, false);
        v.findViewById(R.id.button3).setOnClickListener(this);
        mProgBar = (ProgressBar)v.findViewById(R.id.progressBar2);
        txt_saldomu = (TextView)v.findViewById(R.id.textView2);

        //inisialisasi switch yang nentuin dev atau production di kiri bawah
        sw_prod = (Switch) v.findViewById(R.id.switch2);
        sw_prod.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                MyApiClient.IS_PROD = isChecked;
                if(MyApiClient.IS_PROD)
                    MyApiClient.headaddressfinal = MyApiClient.headaddressPROD;
                else
                    MyApiClient.headaddressfinal = MyApiClient.headaddressDEV;
                MyApiClient.initializeAddress();
                Log.d("isi ISPROD", String.valueOf(MyApiClient.IS_PROD));
            }
        });
        return v;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        resultReceiever = new ServiceReceiver(new Handler());
        resultReceiever.setReceiver(this);

        mProgress = 0;

        if(MyApiClient.IS_PROD)
            MyApiClient.headaddressfinal = MyApiClient.headaddressPROD;
        else
            MyApiClient.headaddressfinal = MyApiClient.headaddressDEV;

        //tiap button diklik akan keluar dialog ini
        alertbox = new AlertDialog.Builder(getActivity());
        alertbox.setTitle("Populate Realm");
        alertbox.setMessage("Apakah yakin membuat baru File Realm ini ?");
        alertbox.setPositiveButton("Ok", new
                DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {

                        mProgBar.setVisibility(View.VISIBLE);
                        mProgress = 0;
                        mProgBar.setProgress(mProgress);
                        Intent startIntent = new Intent(getActivity(),
                                BBSMemberPopulateService.class);

                        // isi intent dengan data sesuai dengan aplikasi yg dipilih
                        // berdasarkan button_id nya
                        // datanya berupa comm_id, path webservicenya, realm version dan realm namenya
                        switch (button_id){
                            case R.id.button3:
                                txt_saldomu.setVisibility(View.GONE);
                                if(!MyApiClient.IS_PROD){
                                    MyApiClient.COMM_ID = MyApiClient.COMMID_DEV_HPKU;
                                    MyApiClient.address = MyApiClient.headaddressfinal+MyApiClient.HPKU;
                                    startIntent.putExtra("realmName", "saldomu"+suffixDevName);
                                    startIntent.putExtra("realmVer",SALDOMU_VER_DEV);

                                }
                                else {
                                    MyApiClient.COMM_ID = MyApiClient.COMMID_PROD_HPKU;
                                    MyApiClient.address = MyApiClient.headaddressfinal+MyApiClient.HPKU;
                                    startIntent.putExtra("realmName", "saldomu"+suffixProdName);
                                    startIntent.putExtra("realmVer",SALDOMU_VER_PROD);
                                }
                                startIntent.putExtra("app_id",SALDOMU_APP_ID);
                                startIntent.putExtra("appType",SALDOMU_VER_PROD);
                                break;
                        }
                        MyApiClient.initializeAddress();
                        createTheRealm(startIntent);
                    }
                });
        alertbox.setNegativeButton("Cancel", new
                DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {}
                });



        mProgBar.setMax(100);
        mProgBar.setProgress(0);
        mProgBar.setIndeterminate(false);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && getActivity().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSIONS_REQ_WRITEEXTERNALSTORAGE);
        }
    }

    @Override
    public void onClick(View v) {
        button_id = v.getId();
        showDialog();

    }


    private void showDialog(){
        if(alertbox != null)
            alertbox.show();
    }

    private void createTheRealm(Intent startIntent){
        startIntent.putExtra("receiver", resultReceiever);
        getActivity().startService(startIntent);
    }

    /**
     * Interface onReceiveResult yang ditrigger dari service PopulateService.java
     * isinya nentuin UI jika dapet result yg error , sukses atau sedang on progress
     * @param resultCode
     * @param resultData
     */
    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        switch (resultCode){
            case PopulateService.DOWNLOAD_ERROR:
                Boolean isContinue = resultData.getBoolean("isContinue",false);
                Toast.makeText(getActivity(),resultData.getString("msg",""),Toast.LENGTH_SHORT).show();
                if(!isContinue) {
                    mProgBar.setVisibility(View.GONE);
                    mProgBar.setProgress(0);
                }
                break;
            case PopulateService.DOWNLOAD_SUCCESS:
                Toast.makeText(getActivity(),"Realm file selesai",Toast.LENGTH_SHORT).show();
                mProgBar.setProgress(mProgress+100);
                switch (resultData.getInt("appType")){
                    case SALDOMU  :
                        txt_saldomu.setVisibility(View.VISIBLE);
                        break;
                }
                break;
            case PopulateService.DOWNLOAD_PROGRESS:
                if(mProgBar.getProgress() <= 98)
                    mProgBar.setProgress(++mProgress);
                break;
        }
    }

}
