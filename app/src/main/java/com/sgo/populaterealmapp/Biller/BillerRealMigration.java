package com.sgo.populaterealmapp.Biller;

import io.realm.DynamicRealm;
import io.realm.FieldAttribute;
import io.realm.RealmMigration;
import io.realm.RealmObjectSchema;
import io.realm.RealmSchema;

/**
 * Created by yuddistirakiki on 4/5/16.
 *
 * Biller migration jika ada perubahan strukstur model bisnis
 */
public class BillerRealMigration implements RealmMigration {
    @Override
    public void migrate(DynamicRealm realm, long oldVersion, long newVersion) {

        RealmSchema schema = realm.getSchema();

        if (oldVersion == 0) {
            schema.get("Account_Collection_Model")
                    .setRequired("comm_id",true);

            schema.get("bank_biller_model")
                    .setRequired("product_code",true);

            RealmObjectSchema billerDataModel = schema.get("Biller_Data_Model");
            billerDataModel.addField("manual_advice",String.class,FieldAttribute.REQUIRED);
        }
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
