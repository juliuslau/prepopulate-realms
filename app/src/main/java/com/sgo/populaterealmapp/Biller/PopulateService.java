package com.sgo.populaterealmapp.Biller;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.ResultReceiver;
import android.util.Log;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.sgo.populaterealmapp.Beans.Account_Collection_Model;
import com.sgo.populaterealmapp.Beans.Biller_Data_Model;
import com.sgo.populaterealmapp.Beans.Biller_Type_Data_Model;
import com.sgo.populaterealmapp.Beans.Denom_Data_Model;
import com.sgo.populaterealmapp.Beans.bank_biller_model;
import com.sgo.populaterealmapp.MyApiClient;
import com.sgo.populaterealmapp.WebParams;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmList;
import io.realm.RealmResults;
import io.realm.annotations.RealmModule;

/**
 * Created by yuddistirakiki on 5/10/16.
 * Intent Service yang membuat db realm.
 * semua prosesnya synchronous
 *
 */
public class PopulateService extends IntentService {

    public static final int DOWNLOAD_ERROR = 10;
    public static final int DOWNLOAD_SUCCESS = 11;
    public static final int DOWNLOAD_PROGRESS = 12;

    ResultReceiver receiver;
    Realm realm;
    int appType;
    Bundle oi;
    File folder;

    @RealmModule(classes = { Account_Collection_Model.class, bank_biller_model.class, Biller_Data_Model.class, Biller_Type_Data_Model.class,
            Denom_Data_Model.class})
    private static class BillerModule {
    }

    public PopulateService() {
        super("PopulateService");
    }

    public PopulateService(String name) {
        super(name);
    }

    /**
     * Path dimana file realm nanti disimpan
     */
    private void createFolder(){
        folder = new File(Environment.getExternalStorageDirectory() +
                File.separator + "PopulateRealmApp");
        if (!folder.exists()) {
            folder.mkdir();
        }

    }

    /**
     * servicenya dimulai dari sini
     * inisialisai RealmConfiguration dengan data yang dikirim dari fragment tadi
     * buat juga path folder tempat file realm nanti disimpat di internal storage
     * simpan juga receiver yang dikirim dari activity
     * @param intent
     */
    @Override
    protected void onHandleIntent(Intent intent) {
        createFolder();

        appType = intent.getIntExtra("appType",0);
        oi = new Bundle();

        RealmConfiguration config = new RealmConfiguration.Builder()
                .directory(folder)
                .name(intent.getStringExtra("realmName"))
                .schemaVersion(intent.getIntExtra("realmVer",0))
                .migration(new BillerRealMigration())
                .modules(new BillerModule())
                .build();

        realm = Realm.getInstance(config);

        receiver = intent.getParcelableExtra("receiver");
        getBiller();
    }


    /**
     * Web service pertama yang dipanggil adalah billertype
     */
    public void getBiller(){
        try{

            receiver.send(DOWNLOAD_PROGRESS,null);
            MyApiClient.getBillerType(this, true,new JsonHttpResponseHandler() {

                @Override
                public void onProgress(long bytesWritten, long totalSize) {
                    super.onProgress(bytesWritten, totalSize);
                    receiver.send(DOWNLOAD_PROGRESS,null);
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    receiver.send(DOWNLOAD_PROGRESS,null);
                    try {
                        String code = response.getString(WebParams.ERROR_CODE);
                        Log.d("Isi response get Biller Type:" ,response.toString());
                        if (code.equals(WebParams.SUCCESS_CODE)) {
                            receiver.send(DOWNLOAD_PROGRESS,null);
                            String arrayBiller = response.getString(WebParams.BILLER_TYPE_DATA);
                            getDataCollection(new JSONArray(arrayBiller));
                        } else {
                            code = response.getString(WebParams.ERROR_MESSAGE);
                            oi.putString("msg",code);
                            EndError(DOWNLOAD_ERROR,oi);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                private void failure(Throwable throwable) {
                    Log.w("Error Koneksi biller data list buy:" , throwable.toString());
                    oi.putString("msg",throwable.toString());
                    EndError(DOWNLOAD_ERROR,oi);
                }
            });
        }catch (Exception e){
            Log.d("httpclient:",e.getMessage());
        }
    }

    /**
     * kedua panggil account collection (biasanya gak ada data)
     * @param arrayBiller
     */
    public void getDataCollection(final JSONArray arrayBiller){
        try{

            receiver.send(DOWNLOAD_PROGRESS,null);
            RequestParams params = new RequestParams();
            params.put(WebParams.RC_UUID, "123456");
            params.put(WebParams.RC_DTIME, "123456");
            params.put(WebParams.SIGNATURE, "123456");

            params.put(WebParams.CUSTOMER_ID, MyApiClient.USER_ID);
            params.put(WebParams.USER_ID, MyApiClient.USER_ID);
            params.put(WebParams.DATETIME, DateTimeFormat.getCurrentDateTime());
            params.put(WebParams.COMM_ID, MyApiClient.COMM_ID);

            Log.d("Isi params CommAccountCollection:" , params.toString());

            MyApiClient.sentCommAccountCollection(this,true,params, new JsonHttpResponseHandler() {

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    receiver.send(DOWNLOAD_PROGRESS,null);
                    try {
                        Log.d("Isi response CommAccountCollection:" , response.toString());
                        String code = response.getString(WebParams.ERROR_CODE);
                        if (code.equals(WebParams.SUCCESS_CODE) || code.equals("0003")) {
                            receiver.send(DOWNLOAD_PROGRESS,null);
                            JSONArray arrayCollection ;
                            String data = response.getString(WebParams.COMMUNITY);
                            if(code.equals("0003")||data.equals("")){
                                arrayCollection = new JSONArray();
                            }
                            else
                                arrayCollection = new JSONArray(data);
                            initializeData(arrayBiller,arrayCollection);
                        }
                        else {
                            String message = response.getString(WebParams.ERROR_MESSAGE);
                            oi.putString("msg",message);
                            EndError(DOWNLOAD_ERROR,oi);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                private void failure(Throwable throwable){
                    Log.w("Error Koneksi collect data list buy:",throwable.toString());
                    oi.putString("msg",throwable.toString());
                    EndError(DOWNLOAD_ERROR,oi);
                }
            });
        }catch (Exception e){
            Log.d("httpclient:" , e.getMessage());
        }
    }


    /**
     * mulai insert data biller
     * prosesnya dimulai dengan menyimpan data Biller Type Data (jika ada)
     * delete biller type data
     * insert data biller type data yang baru
     * pindahin data biller ke data biller type data yang baru
     * panggil web service biller espay
     * hapus biller data yg lama sesuai dengan biller type data yg baru
     * insert data biller yang baru
     * selesai
     *
     * biller data termasuk didalamanya ada denom dan list bank
     *
     * @param arrayBiller
     * @param arrayCollection
     */
    private void initializeData(final JSONArray arrayBiller, final JSONArray arrayCollection){
        receiver.send(DOWNLOAD_PROGRESS,null);
        final String curr_date = DateTimeFormat.getCurrentDate();

        Biller_Type_Data_Model mBillerObj;
        RealmResults<Biller_Type_Data_Model> ResultData = realm.where(Biller_Type_Data_Model.class).findAll();
        List<Biller_Type_Data_Model> temptData = realm.copyFromRealm(ResultData);

        realm.beginTransaction();

        if (arrayBiller.length() > 0) {
            receiver.send(DOWNLOAD_PROGRESS,null);
            realm.delete(Biller_Type_Data_Model.class);
            Biller_Data_Model mListBillerData;
            int i;
            int j;
            int k;
            for ( i= 0; i < arrayBiller.length(); i++) {
                receiver.send(DOWNLOAD_PROGRESS,null);
                try {
                    mBillerObj = realm.createObjectFromJson(Biller_Type_Data_Model.class, arrayBiller.getJSONObject(i));
                    mBillerObj.setLast_update(curr_date);

                    for (j = 0; j < temptData.size(); j++) {
                        receiver.send(DOWNLOAD_PROGRESS,null);
                        if (mBillerObj.getBiller_type_code().equals(temptData.get(j).getBiller_type_code())) {
                            for ( k = 0; k < temptData.get(j).getBiller_data_models().size(); k++) {
                                mListBillerData = realm.where(Biller_Data_Model.class).
                                        equalTo("comm_id", temptData.get(j).getBiller_data_models().get(k).getComm_id()).
                                        equalTo("comm_name", temptData.get(j).getBiller_data_models().get(k).getComm_name()).
                                        findFirst();
                                mBillerObj.getBiller_data_models().add(mListBillerData);
                            }
                            break;
                        }
                    }

                    getBillerData(mBillerObj.getBiller_type_code());

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            Boolean isHave = false;
            Biller_Data_Model delList;
            for(i = 0 ; i < temptData.size() ; i++){
                for (j = 0 ; j < ResultData.size() ; j++){
                    if(temptData.get(i).getBiller_type_code().equals(ResultData.get(j).getBiller_type_code())){
                        isHave = true;
                    }
                }

                receiver.send(DOWNLOAD_PROGRESS,null);
                if(!isHave){
                    for ( j = 0; j < temptData.get(i).getBiller_data_models().size(); j++) {
                        delList = realm.where(Biller_Data_Model.class).
                                equalTo("comm_id", temptData.get(i).getBiller_data_models().get(j).getComm_id()).
                                equalTo("comm_name", temptData.get(i).getBiller_data_models().get(j).getComm_name()).
                                findFirst();
                        if(delList.getDenom_data_models().size()>0)
                            delList.getDenom_data_models().deleteAllFromRealm();
                        delList.deleteFromRealm();
                    }
                }
                isHave = false;
            }
        }

        receiver.send(DOWNLOAD_PROGRESS,null);

        realm.delete(Account_Collection_Model.class);
        if (arrayCollection.length() > 0) {
            Account_Collection_Model mACLobj;
            try {
                for (int i = 0; i < arrayCollection.length(); i++) {
                    receiver.send(DOWNLOAD_PROGRESS,null);
                    mACLobj = realm.createObjectFromJson(Account_Collection_Model.class, arrayCollection.getJSONObject(i));
                    mACLobj.setLast_update(curr_date);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        realm.commitTransaction();

        oi.putInt("appType",appType);
        EndError(DOWNLOAD_SUCCESS,oi);
    }

    public void getBillerData(final String _biller_type_code){
        try{
            RequestParams params = new RequestParams();
            params.put(WebParams.RC_UUID, "123456");
            params.put(WebParams.RC_DTIME, "123456");
            params.put(WebParams.SIGNATURE, "123456");

            params.put(WebParams.BILLER_TYPE, _biller_type_code);
            params.put(WebParams.USER_ID, MyApiClient.USER_ID);
            params.put(WebParams.COMM_ID, MyApiClient.COMM_ID);

            Log.d("isi params get biller list merchantnya:" ,params.toString());

            MyApiClient.sentListBiller(this,true,params,new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        String code = response.getString(WebParams.ERROR_CODE);
                        Log.d("Isi response get Biller list:" , response.toString());
                        if (code.equals(WebParams.SUCCESS_CODE)) {
                            final String arrayBiller = response.getString(WebParams.BILLER_DATA);
                            insertUpdateData(new JSONArray(arrayBiller),_biller_type_code);
                        }
                        else if(code.equals("0003")){

                        }
                        else {
                            code = response.getString(WebParams.ERROR_MESSAGE);
                            oi.putString("msg",code);
                            EndError(DOWNLOAD_ERROR,oi);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                private void failure(Throwable throwable){
                    Log.w("Error Koneksi biller list tabbuyitem:",throwable.toString());
                    oi.putString("msg",throwable.toString());
                    EndError(DOWNLOAD_ERROR,oi);
                }
            });
        }catch (Exception e){
            Log.d("httpclient:",e.getMessage());
        }
    }

    private void insertUpdateData(final JSONArray arrayBiller, final String _biller_type_code){

        if(!realm.isInTransaction())
            realm.beginTransaction();

        Biller_Type_Data_Model mBillerTypeData = realm.where(Biller_Type_Data_Model.class)
                .equalTo(WebParams.BILLER_TYPE_CODE, _biller_type_code)
                .findFirst();

        RealmList<Biller_Data_Model> ResultBillerData = mBillerTypeData.getBiller_data_models();

        if (ResultBillerData.size() > 0) {
            Biller_Data_Model mObj;
            List<Biller_Data_Model> refObj = realm.copyFromRealm(ResultBillerData);
            Denom_Data_Model mObjDenom;
            List<Denom_Data_Model> refObjDenom;

            for (int i = 0; i < refObj.size(); i++) {
                mObj = realm.where(Biller_Data_Model.class).
                        equalTo(WebParams.COMM_ID, refObj.get(i).getComm_id()).
                        equalTo(WebParams.COMM_NAME, refObj.get(i).getComm_name()).
                        findFirst();
                if (mObj.getItem_id().isEmpty()) {
                    //Delete Denom on Table Denom
                    refObjDenom = realm.copyFromRealm(mObj.getDenom_data_models());
                    for (int k = 0; k < refObjDenom.size(); k++) {
                        String itemid = refObjDenom.get(k).getItem_id();
                        mObjDenom = realm.where(Denom_Data_Model.class).
                                equalTo(WebParams.DENOM_ITEM_ID,itemid).
                                findFirst();
                        mObjDenom.deleteFromRealm();
                    }
                }
                mObj.deleteFromRealm(); //delete BillerData on table billerData
            }


        }

        mBillerTypeData.getBiller_data_models().deleteAllFromRealm(); //delete billerdata on BillerTypeData Obj

        JSONArray jsonData;
        JSONObject mJob;
        String curr_date = DateTimeFormat.getCurrentDate();
        Biller_Data_Model mObj;
        Denom_Data_Model mDenomData;
        bank_biller_model refObj;
        int i;
        int j;
        try {
            if (arrayBiller != null && arrayBiller.length() > 0) {
                for ( i = 0; i < arrayBiller.length(); i++) {
                    mObj = realm.createObjectFromJson(Biller_Data_Model.class, arrayBiller.getJSONObject(i));
                    mObj.setLast_update(curr_date);


                    //add denom ke new biller
                    if(arrayBiller.getJSONObject(i).has(WebParams.DENOM_DATA)) {
                        jsonData = new JSONArray(arrayBiller.getJSONObject(i).optString(WebParams.DENOM_DATA, ""));
                        if (jsonData.length() > 0) {
                            for (j = 0; j < jsonData.length(); j++) {
                                mJob = jsonData.getJSONObject(j);
                                mDenomData = new Denom_Data_Model();
                                mDenomData.setItem_id(mJob.optString(WebParams.DENOM_ITEM_ID));
                                mDenomData.setItem_name(mJob.optString(WebParams.DENOM_ITEM_NAME));
                                mDenomData.setItem_price(mJob.optString(WebParams.DENOM_ITEM_PRICE));
                                mDenomData.setCcy_id(mJob.optString(WebParams.DENOM_CCY_ID));
                                mDenomData.setLast_update(curr_date);

//                            realm.copyToRealm(mDenomData);
                                mObj.getDenom_data_models().add(mDenomData);
                            }
                        }
                    }

                    //add bank biller atau update jika sudah ada di table bank biller
                    if(arrayBiller.getJSONObject(i).has(WebParams.BANK_BILLER)) {
                        jsonData = new JSONArray(arrayBiller.getJSONObject(i).optString(WebParams.BANK_BILLER, ""));
                        if (jsonData.length() > 0) {
                            for (j = 0; j < jsonData.length(); j++) {
                                mJob = jsonData.getJSONObject(j);
                                refObj = new bank_biller_model();
                                refObj.setBank_code(mJob.optString(WebParams.BANK_CODE, ""));
                                refObj.setBank_name(mJob.optString(WebParams.BANK_NAME, ""));
                                refObj.setProduct_code(mJob.optString(WebParams.PRODUCT_CODE, ""));
                                refObj.setProduct_name(mJob.optString(WebParams.PRODUCT_NAME, ""));
                                refObj.setProduct_type(mJob.optString(WebParams.PRODUCT_TYPE, ""));
                                refObj.setProduct_h2h(mJob.optString(WebParams.PRODUCT_H2H, ""));
                                refObj.setLast_update(curr_date);

                                realm.copyToRealmOrUpdate(refObj);
                                mObj.getBank_biller_models().add(refObj);
                            }
                        }
                    }

                    mBillerTypeData.getBiller_data_models().add(mObj);
                    Log.d("isi array biller realm idx : " , arrayBiller.getJSONObject(i).getString(WebParams.COMM_ID));
                }


            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void EndRealm(){
        if(realm.isInTransaction())
            realm.cancelTransaction();

        if(realm != null && !realm.isClosed())
            realm.close();
    }

    private void EndError(int ReceiverTag, Bundle io){
        Bundle buk = (Bundle)io.clone();
        io.clear();
        EndRealm();
        receiver.send(ReceiverTag,buk);

    }
}
